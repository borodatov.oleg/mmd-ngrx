import { Store, select } from '@ngrx/store';
import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Observable } from 'rxjs';

import {
  routeAnimations,
  selectIsAuthenticated
} from '../../../core/core.module';

import { State } from '../examples.state';

@Component({
  selector: 'mmd-examples',
  templateUrl: './examples.component.html',
  styleUrls: ['./examples.component.scss'],
  animations: [routeAnimations],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExamplesComponent implements OnInit {
  isAuthenticated$: Observable<boolean>;

  examples = [
    { link: 'todos', label: 'mmd.examples.menu.todos' },
    { link: 'stock-market', label: 'mmd.examples.menu.stocks' },
    { link: 'theming', label: 'mmd.examples.menu.theming' },
    { link: 'crud', label: 'mmd.examples.menu.crud' },
    {
      link: 'simple-state-management',
      label: 'mmd.examples.menu.simple-state-management'
    },
    { link: 'form', label: 'mmd.examples.menu.form' },
    { link: 'notifications', label: 'mmd.examples.menu.notifications' },
    { link: 'elements', label: 'mmd.examples.menu.elements' },
    { link: 'authenticated', label: 'mmd.examples.menu.auth', auth: true }
  ];

  constructor(private store: Store<State>) {}

  ngOnInit(): void {
    this.isAuthenticated$ = this.store.pipe(select(selectIsAuthenticated));
  }
}
